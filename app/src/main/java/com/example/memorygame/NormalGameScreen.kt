package com.example.memorygame

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider

class NormalGameScreen : AppCompatActivity(), View.OnClickListener {
    private lateinit var card1 : ImageView
    private lateinit var card2 : ImageView
    private lateinit var card3 : ImageView
    private lateinit var card4 : ImageView
    private lateinit var card5 : ImageView
    private lateinit var card6 : ImageView
    private lateinit var card7 : ImageView
    private lateinit var card8 : ImageView

    private val difficulty: String = "Normal"

    lateinit var viewModel: NormalGameViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.normal_game_screen)
        viewModel = ViewModelProvider(this).get(NormalGameViewModel::class.java)

        viewModel.moveCounter = findViewById(R.id.moviments_count)
        viewModel.moves = viewModel.moveCounter.text.toString().toInt()

        card1 = findViewById(R.id.card_1)
        card2 = findViewById(R.id.card_2)
        card3 = findViewById(R.id.card_3)
        card4 = findViewById(R.id.card_4)
        card5 = findViewById(R.id.card_5)
        card6 = findViewById(R.id.card_6)
        card7 = findViewById(R.id.card_7)
        card8 = findViewById(R.id.card_8)

        card1.setOnClickListener(this)
        card2.setOnClickListener(this)
        card3.setOnClickListener(this)
        card4.setOnClickListener(this)
        card5.setOnClickListener(this)
        card6.setOnClickListener(this)
        card7.setOnClickListener(this)
        card8.setOnClickListener(this)

        updateUI()


    }

    override fun onClick(v: View?) {
        when(v) {
            card1 -> {
                viewModel.girarCarta(0, card1)
                viewModel.compararCartes(0, card1)
                if (viewModel.score >= 4){
                    goToResult()
                } else if (viewModel.moves <= 0){
                    goToResult()
                }
            }
            card2 -> {
                viewModel.girarCarta(1, card2)
                viewModel.compararCartes(1, card2)
                if (viewModel.score >= 4){
                    goToResult()
                } else if (viewModel.moves <= 0){
                    goToResult()
                }
            }
            card3 -> {
                viewModel.girarCarta(2, card3)
                viewModel.compararCartes(2, card3)
                if (viewModel.score >= 4){
                    goToResult()
                } else if (viewModel.moves <= 0){
                    goToResult()
                }
            }
            card4 -> {
                viewModel.girarCarta(3, card4)
                viewModel.compararCartes(3, card4)
                if (viewModel.score >= 4){
                    goToResult()
                } else if (viewModel.moves <= 0){
                    goToResult()
                }
            }
            card5 -> {
                viewModel.girarCarta(4, card5)
                viewModel.compararCartes(4, card5)
                if (viewModel.score >= 4){
                    goToResult()
                } else if (viewModel.moves <= 0){
                    goToResult()
                }
            }
            card6 -> {
                viewModel.girarCarta(5, card6)
                viewModel.compararCartes(5, card6)
                if (viewModel.score >= 4){
                    goToResult()
                } else if (viewModel.moves <= 0){
                    goToResult()
                }
            }
            card7 -> {
                viewModel.girarCarta(6, card7)
                viewModel.compararCartes(6, card7)
                if (viewModel.score >= 4){
                    goToResult()
                } else if (viewModel.moves <= 0){
                    goToResult()
                }
            }
            card8 -> {
                viewModel.girarCarta(7, card8)
                viewModel.compararCartes(7, card8)
                if (viewModel.score >= 4){
                    goToResult()
                } else if (viewModel.moves <= 0){
                    goToResult()
                }
            }
        }
    }

    private fun goToResult() {
        viewModel.calculateQualification()
        val intent = Intent(this, ResultScreen::class.java)
        intent.putExtra("Qualification", viewModel.qualification)
        intent.putExtra("Moves", viewModel.moves)
        intent.putExtra("Difficulty", difficulty)
        startActivity(intent)
    }

    private fun updateUI() {
        card1.setImageResource(viewModel.estatCarta(0))
        card2.setImageResource(viewModel.estatCarta(1))
        card3.setImageResource(viewModel.estatCarta(2))
        card4.setImageResource(viewModel.estatCarta(3))
        card5.setImageResource(viewModel.estatCarta(4))
        card6.setImageResource(viewModel.estatCarta(5))
        card7.setImageResource(viewModel.estatCarta(6))
        card8.setImageResource(viewModel.estatCarta(7))

    }
}