package com.example.memorygame

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class MenuScreen : AppCompatActivity() {
    lateinit var helpButton: Button
    lateinit var playButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.Theme_MemoryGame)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.menu_screen)

        val spinner : Spinner = findViewById<Spinner>(R.id.difficulty)
        ArrayAdapter.createFromResource(
            this,
            R.array.Difficulties,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner.adapter = adapter
        }

        helpButton = findViewById(R.id.help_button)
        helpButton.setOnClickListener{
            MaterialAlertDialogBuilder(this,
                R.style.ThemeOverlay_MaterialComponents_Dialog)
                .setMessage(resources.getString(R.string.help_text))
                .setPositiveButton(resources.getString(R.string.understood)) { dialog, which ->
                    // Respond to positive button press
                }
                .show()
        }

        playButton = findViewById(R.id.play_button)
        playButton.setOnClickListener{
            when (spinner.selectedItem.toString()) {
                "Easy" -> startActivity(Intent(this, EasyGameScreen::class.java))
                "Normal" -> startActivity(Intent(this, NormalGameScreen::class.java))
            }
        }
    }
}
