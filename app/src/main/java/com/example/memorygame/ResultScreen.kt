package com.example.memorygame

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class ResultScreen : AppCompatActivity() {
    lateinit var resultText: TextView
    lateinit var playAgainButton: Button
    lateinit var menuButton: Button

    var qualification: String? = null
    var moves: Int = 0

    var difficulty: String? = null

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.results_screen)

        resultText = findViewById(R.id.result_text)
        playAgainButton = findViewById(R.id.play_again_button)
        menuButton = findViewById(R.id.menu_button)

        qualification = intent.getStringExtra("Qualification")
        moves = intent.getIntExtra("Moves", 0)
        difficulty = intent.getStringExtra("Difficulty")

        val text = "Qualification: $qualification\nMoves left: $moves"
        resultText.text = text

        playAgainButton.setOnClickListener{
            when (difficulty){
                "Easy" -> startActivity(Intent(this, EasyGameScreen::class.java))
                "Normal" -> startActivity(Intent(this, NormalGameScreen::class.java))
            }
        }
        menuButton.setOnClickListener{
            startActivity(Intent(this, MenuScreen::class.java))
        }

    }
}