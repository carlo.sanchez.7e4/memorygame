package com.example.memorygame

import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.ViewModel

class GameViewModel : ViewModel() {
    private var images = arrayOf(R.drawable.young_link, R.drawable.zelda, R.drawable.ganondorf, R.drawable.young_link, R.drawable.zelda, R.drawable.ganondorf)
    private var cards = mutableListOf<Card>()

    var moves: Int = 0
    var score: Int = 0
    var qualification: String? = null

    private var firstClick: Int? = null
    private var firstImage: ImageView? = null
    private var secondClick: Int? = null
    private var secondImage: ImageView? = null
    lateinit var moveCounter: TextView

    init {
        setDataModel()
    }

    fun compararCartes(cardId: Int, card: ImageView) {
        if (firstClick == null){
            firstClick = cardId
            firstImage = card
            firstImage?.isEnabled = false
        } else if (secondClick == null && secondClick != firstClick){
            secondClick = cardId
            secondImage = card
            if (cards[firstClick!!].resId == cards[secondClick!!].resId){
                firstImage?.setImageResource(R.drawable.card_matched)
                cards[firstClick!!].checked = true
                secondImage?.setImageResource(R.drawable.card_matched)
                cards[secondClick!!].checked = true
                firstImage?.isEnabled = false
                secondImage?.isEnabled = false
                clearSelected()
                score++
            } else {
                girarCarta(firstClick!!, firstImage!!)
                girarCarta(secondClick!!, secondImage!!)
                firstImage?.isEnabled = true
                clearSelected()
                moves--
                moveCounter.setText(moves.toString())
            }
        }
    }

    fun calculateQualification() {
        when (moves) {
            3 -> {
                qualification = "PERFECT!"
            }
            2 -> {
                qualification = "Nice"
            }
            1 -> {
                qualification = "Good"
            }
            0 -> {
                qualification = "YOU LOST"
            }
        }
    }

    fun clearSelected() {
        firstClick = null
        secondClick = null
        firstImage = null
        secondImage = null
    }

    fun setDataModel() {
        images.shuffle()
        for(i in 0..5) {
            cards.add(Card(i, images[i]))
        }
    }

    fun girarCarta(idCarta: Int, carta: ImageView, giradaInicial: Boolean = false)  {
        if (giradaInicial) {
            carta.setImageResource(R.drawable.card_backside)
            cards[idCarta].girada = false
        } else {
            if (!cards[idCarta].girada) {
                carta.setImageResource(cards[idCarta].resId)
                cards[idCarta].girada = true
            } else {
                carta.setImageResource(R.drawable.card_backside)
                cards[idCarta].girada = false
            }
        }
    }

    fun estatCarta(idCarta: Int): Int {
        if (cards[idCarta].checked) {
            return R.drawable.card_matched
        } else if (cards[idCarta].girada) {
            return cards[idCarta].resId
        } else {
            return R.drawable.card_backside
        }
    }
}