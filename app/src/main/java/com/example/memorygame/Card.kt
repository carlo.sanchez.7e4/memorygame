package com.example.memorygame

data class Card(val id: Int, val resId: Int, var girada: Boolean = false, var checked: Boolean = false)